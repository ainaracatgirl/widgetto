![](./repo.png)

# Widgetto
Widgetto is a collection of standard UI components and design practices.

1. Obtain the [stylesheet](./widgetto.css)
2. Remove everything you won't be using to minimize the footprint of your site
This includes the `ic` and `ic-heart` classes, and possibly the view transiton classes.
3. Customize the colors:
```css
:root {
  /* all of these should be the same tone, even if slightly adjusted */
  --bg: #000; /* background color */
  --fg: #fff; /* text & element color */
  --di: #9aa; /* disabled element color */

  /* additionally, you can add a second tone to be used as accent (use the "accent" class) */
  /* if you prefer to not use an accent color, set this to the same as --fg */
  --ac: #aee;
}
```
